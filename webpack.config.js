var webpack = require("webpack");
var path = require("path");

const PORT = (process.env.PORT || 3000);
const NODE_ENV = (process.env.NODE_ENV || "development");

module.exports = {
  entry: {
    inject: path.join(__dirname, "src", "inject.js"),
    background: path.join(__dirname, "src", "background.js")
  },
  output: {
    path: path.join(__dirname, "extension", "scripts"),
    filename: "[name].bundle.js"
  },
  plugins: NODE_ENV === 'production' ? [
    new webpack.optimize.UglifyJsPlugin({
      compress: {warnings: false}
    })
  ] : [],
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: [
            'es2015',
            'stage-0', 'stage-1', 'stage-2'
          ],
          plugins: ['transform-decorators-legacy']
        }
      }
    ]
  }
};
