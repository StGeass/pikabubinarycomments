import MediumEditor from 'medium-editor';
import rangy from 'rangy';
import rangyApplier from 'rangy/lib/rangy-classapplier';

const isMobile = window.location.hostname[0] === 'm';
const postSelector = isMobile ? '.comment__content' : '.b-comment__content';
const replySelector = isMobile ? '.comment_new_form' : '.b-comments-reply';
const submitSelector = isMobile ? '.comment_new_form_send' : '.b-comments-reply__submit';
const wysiwygSelector = isMobile ? '.form_input' : '.b-comments-reply__editable';

const binaryLabelClass = 'pbc-editor-b8';
const base64LabelClass = 'pbc-editor-b64';
const modifiedNodeClass = 'pbc-modified';
const modifiedCommentClass = 'binary-decoded';
const maxDeep = 15;

const binaryRule = /(\s*[01]{6,8}\s*)+/g;

const pagesWhiteList = [
  /^\/story/,
  /^\/commented/,
  /^\/freshitems/
];

chrome.runtime.sendMessage({method: 'getLocalStorage'}, function (res) {
  const pathname = window.location.pathname;

  if (res.globalStatus && pagesWhiteList.some(rule => rule.test(pathname))) {
    let observer = new MutationObserver(() => observeAndModify());
    let commentsContainer = isMobile
      ? document.getElementsByClassName('post_comments')[0]
      : document.getElementsByClassName('inner_wrap')[0];

    observer.observe(commentsContainer, {
      childList: true,
      subtree: true
    });

    observeAndModify();
  }
});

function observeAndModify() {
  const encoders = [
    escapeNewLines,
    encodeBinary,
    encodeBase64
  ];

  modifyReplyForm();
  modifyBinaryComments();

  function modifyReplyForm() {
    let replyLinks = [...document.querySelectorAll(`${replySelector}:not(.${modifiedCommentClass})`)];
    let activeReply = replyLinks.filter(reply => reply.style.display !== 'none');

    if (activeReply.length) {
      activeReply.forEach((replyBlock) => {
        let fakeButton = createFakeButton();
        let {fakeWysiwyg, fakeWysiwygInstance} = createFakeWysiwyg();
        let realButton = replyBlock.querySelector(submitSelector);
        let realWysiwyg = replyBlock.querySelector(wysiwygSelector);
        let wysiwygObserver = new MutationObserver(() => {
          if (fakeWysiwyg.innerHTML !== realWysiwyg.innerHTML) {
            fakeWysiwyg.innerHTML = realWysiwyg.innerHTML;
          }
        });

        wysiwygObserver.observe(realWysiwyg, {
          childList: true,
          subtree: true
        });

        fakeButton.addEventListener('click', (e) => {
          if (isMobile) {
            realWysiwyg.value = encodeBinary(escapeNewLines(fakeWysiwyg.value));
          } else {
            realWysiwyg.innerHTML = encoders.reduce((html, encoder) => encoder(html), fakeWysiwyg.innerHTML);
          }

          fakeWysiwyg.innerText = '';

          realButton.click();
        });

        fakeWysiwyg.addEventListener('keydown', (e) => {
          if ((e.ctrlKey || e.metaKey) && (e.keyCode == 13 || e.keyCode == 10)) {
            if (isMobile) {
              realWysiwyg.value = encodeBinary(escapeNewLines(fakeWysiwyg.value));
            } else {
              realWysiwyg.innerHTML = encoders.reduce((html, encoder) => encoder(html), fakeWysiwyg.innerHTML);
            }

            fakeWysiwyg.innerText = '';

            realButton.click();
          }
        });

        fakeWysiwygInstance.subscribe('editableInput', function (event, editable) {
          if (fakeWysiwyg.innerHTML !== realWysiwyg.innerHTML) {
            realWysiwyg.innerHTML = fakeWysiwyg.innerHTML;
          }
        });

        replyBlock.classList.add(modifiedCommentClass);

        realButton.style.display = 'none';
        realButton.parentNode.insertBefore(fakeButton, realButton.nextSibling);

        realWysiwyg.style.left = '-10000px';
        realWysiwyg.style.position = 'absolute';
        realWysiwyg.parentNode.insertBefore(fakeWysiwyg, realWysiwyg.nextSibling);

        if (realWysiwyg.innerText) {
          fakeWysiwyg.innerHTML = realWysiwyg.innerHTML;

          setTimeout(() => {
            var sel, range;
            if (window.getSelection && document.createRange) {
              range = document.createRange();
              range.selectNodeContents(fakeWysiwyg);
              range.collapse(true);
              sel = window.getSelection();
              sel.removeAllRanges();
              sel.addRange(range);
            } else if (document.body.createTextRange) {
              range = document.body.createTextRange();
              range.moveToElementText(fakeWysiwyg);
              range.collapse(true);
              range.select();
            }
          }, 1);
        }
      });
    }
  }

  function modifyBinaryComments() {
    var posts = [...document.querySelectorAll(`${postSelector}:not(.${modifiedCommentClass})`)];

    if (posts.length) {
      posts.forEach((post) => {
        modifyComment(post);
        post.classList.add(modifiedCommentClass); // Больше не парсим найденные ранее посты
      });
    }
  }

  function modifyComment(node, deep = 0) {
    const isText = node.nodeType === 3;
    const isElement = node.nodeType === 1;
    const isNotImage = isElement && !node.classList.contains("b-p_type_image");
    const isNotModified = isElement && !node.classList.contains(modifiedNodeClass);

    if (isElement && isNotImage && isNotModified) {
      let nodeList = [...node.childNodes];
      let isEndPoint = !node.childElementCount || nodeList.every(node => node.nodeType !== 1 || node.nodeName === 'BR');

      if (isEndPoint) {
        modifyNode(node, /[01]{6,8}/.test(node.innerText) ? decodeBinary : decodeBase64);
      } else if (deep <= maxDeep) {
        [...node.childNodes].forEach(node => modifyComment(node, deep + 1));
      }
    } else if (isText && /[01]{6,8}/.test(node.nodeValue)) {
      modifyTextNode(node, binaryRule, decodeBinary)
    }
  }

  function modifyNode(node, modifier) {
    let isWrapper = node.nodeName === 'I';
    let modifiedResult = modifier(node.innerHTML, !isWrapper);

    if (modifiedResult) {
      if (isWrapper) {
        node.parentNode.replaceChild(modifiedResult, node);
      } else {
        node.innerHTML = modifiedResult;
      }
    }
  }

  function modifyTextNode(node, rule, modifier) {
    var bk = 0;
    node.data.replace(rule, function (...args) {
      var match = args[0];
      var offset = args[args.length - 2];
      var newNode = node.splitText(offset + bk);

      bk -= node.data.length + match.length;
      newNode.data = newNode.data.substr(match.length);

      node.parentNode.insertBefore(modifier(match, false), newNode);

      node.data = match[0] === ' ' ? node.data + ' ' : node.data;
      newNode.data = match[match.length - 1] === ' ' ? ' ' + newNode.data : newNode.data;
    });
    rule.lastIndex = 0;
  }

  function createFakeWysiwyg() {
    let fakeWysiwyg = document.createElement('div');

    fakeWysiwyg.className = 'b-comments-reply__editable';

    var B8Button = MediumEditor.extensions.button.extend({
      name: 'b8',
      tagNames: ['b8'],
      contentDefault: '<strong class="pbc-editor-b8-btn">Binary</strong>',
      contentFA: '<strong class="pbc-editor-b8-btn">Binary</strong>',
      aria: 'b8',
      action: 'b8',
      init: function () {
        MediumEditor.extensions.button.prototype.init.call(this);
        this.classApplier = rangyApplier.createClassApplier('pbc-editor-b8-bg', {
          elementTagName: 'b8',
          normalize: true
        });
      },
      handleClick: function (event) {
        this.classApplier.toggleSelection();
        this.base.checkContentChanged();
      }
    });

    var B64Button = MediumEditor.extensions.button.extend({
      name: 'b64',
      tagNames: ['b64'],
      contentDefault: '<strong class="pbc-editor-b64-btn">Base64</strong>',
      contentFA: '<strong class="pbc-editor-b64-btn">Base64</strong>',
      aria: 'b64',
      action: 'b64',
      init: function () {
        MediumEditor.extensions.button.prototype.init.call(this);
        this.classApplier = rangyApplier.createClassApplier('pbc-editor-b64-bg', {
          elementTagName: 'b64',
          normalize: true
        });
      },
      handleClick: function (event) {
        this.classApplier.toggleSelection();
        this.base.checkContentChanged();
      }
    });

    var fakeWysiwygInstance = new MediumEditor(fakeWysiwyg, {
      placeholder: {
        text: 'Начать писать (Модифицированно)'
      },
      toolbar: {
        buttons: ['bold', 'italic', 'underline', 'quote', 'b8', 'b64']
      },
      extensions: {
        'b8': new B8Button(),
        'b64': new B64Button()
      },
      anchor: {
        placeholderText: "Введите ссылку",
        linkValidation: true,
        targetCheckbox: false
      }
    });

    return {fakeWysiwyg, fakeWysiwygInstance};
  }

  function createFakeButton() {
    let fakeButton = document.createElement('button');

    fakeButton.innerHTML = 'Отправить';
    fakeButton.className = isMobile
      ? 'comment_new_form_send button boldlabel active binary-button'
      : 'b-button b-button_type_default binary-button';

    return fakeButton;
  }

  function wrapLinksAndImages(string) {
    const linkRule = /(http|https|ftp|ftps):\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/;
    const imageRule = /([a-z\-_0-9\/:\.]*\.(jpg|jpeg|png))/gi;

    return string && string.replace(linkRule, (url) => imageRule.test(url) ? imageTemplate(url) : linkTemplate(url));
  }

  function linkTemplate(url) {
    let safeUrl = escapeURL(url);

    return `<a href="${safeUrl}">${safeUrl}</a>`;
  }

  function imageTemplate(url) {
    let safeUrl = escapeURL(url);
    return `<div class="b-p b-p_type_image"><a href="${safeUrl}"><img src="${safeUrl}" class="b-image" /></a></div>`;
  }

  function escapeURL(string) {
    return string.replace(/[^-A-Za-z0-9+&@#/%?=~_|!:,.;\s\n\(\)]+/g, '')
  }

  function escapeNewLines(string) {
    return string
      .replace(/(#b)<\/p><p>/gi, '$1')
      .replace(/<\/p><p>(b#)/gi, '$1')
  }

  function escapeHTML(string) {
    return string && string.replace(/<(?!span class="[\w\d-]+")[^>]*?>/gi, '')
  }

  function createRuleForTag(tagName) {
    return new RegExp(`<${tagName}\\s+(?:class="[\\w\\d-]+")>([\\s\\S]*?)<\/${tagName}>`, 'gm');
  }

  function encodeBinary(text) {
    return text.replace(createRuleForTag('b8'), (string) => {
      let utf8 = encodeUTF8(escapeHTML(string));
      let output = utf8.split('').reduce((acc, char) => {
        let binary = encodeBinaryChar(char);
        return acc.concat(new Array(9 - binary.length).join("0") + binary);
      }, []);

      return output.join(" ");
    })
  }

  function decodeBinary(string, asString) {
    let matchRule = /(\s*[01]{6,8}\s*)+/g;

    if (matchRule.test(string)) {
      let decoded = string.replace(matchRule, (match) => {
        let utf8String = match
          .trim()
          .split(' ')
          .reduce((acc, char) => acc + decodeBinaryChar(char.trim()), '');

        return wrapLinksAndImages(escapeHTML(decodeUTF8(utf8String)));
      });

      return highlightString(binaryLabelClass + '-bg', decoded, asString);
    } else {
      return null;
    }
  }

  function encodeBase64(text) {
    return text.replace(createRuleForTag('b64'), (string) => `<i>${btoa(encodeUTF8(escapeHTML(string)))}</i>`)
  }

  function decodeBase64(text, asString) {
    const base64Rule = /^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/;
    const modifiers = [
      atob,
      decodeUTF8,
      escapeHTML,
      wrapLinksAndImages
    ];

    let clearText = text.replace(/<br>/, '').trim();

    if (base64Rule.test(clearText)) {
      let decoded = modifiers.reduce((text, modifier) => modifier(text), clearText);

      return highlightString(base64LabelClass + '-bg', decoded, asString);
    } else {
      return null;
    }
  }

  function highlightString(labelClass, string, asString = true) {
    let className = modifiedNodeClass + ' ' + labelClass;

    if (asString) {
      let firstSpace = string[0] === ' ' ? ' ' : '';
      let lastSpace = string[string.length - 1] === ' ' ? ' ' : '';

      return `${firstSpace}<span class="${className}">${string.trim()}</span>${lastSpace}`;
    } else {
      let span = document.createElement('span');
      span.className = className;
      span.innerHTML = string;
      return span;
    }
  }

  function encodeUTF8(win1251) {
    try {
      return unescape(encodeURIComponent(win1251));
    } catch (e) {
    }
  }

  function decodeUTF8(utf8) {
    try {
      return decodeURIComponent(escape(utf8))
    } catch (e) {
    }
  }

  function encodeBinaryChar(char) {
    return char.charCodeAt(0).toString(2);
  }

  function decodeBinaryChar(char) {
    return String.fromCharCode(parseInt(char, 2));
  }
}
