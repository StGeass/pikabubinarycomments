'use strict';

const STATUS_ENABLED = 'enabled';
const STATUS_DISABLED = 'disabled';
const currentStatus = getStatus();

setStatus(currentStatus === undefined ? true : Boolean(currentStatus));

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  if (changeInfo && changeInfo.status === 'complete' && tab.url.indexOf('pikabu.ru') > -1) {
    chrome.tabs.executeScript(tabId, {file: 'scripts/inject.bundle.js'});
  }
});

chrome.browserAction.onClicked.addListener((tab) => {
  if (tab.url.indexOf('pikabu.ru') === -1) {
    return;
  }

  toggleStatus();

  chrome.tabs.reload();
});

chrome.runtime.onMessage.addListener(function (req, sender, sendResponse) {
  if (req.method === 'getLocalStorage') {
    sendResponse({
      globalStatus: getStatus()
    });
  }
});

function getStatus() {
  return localStorage.getItem('pikabc_extension_status') === 'true';
}

function setStatus(status) {
  localStorage.setItem('pikabc_extension_status', String(status));

  toggleIcons(status ? STATUS_ENABLED : STATUS_DISABLED);
}

function toggleStatus() {
  setStatus(!getStatus());
}

function toggleIcons(status) {
  chrome.browserAction.setIcon({
    path: {
      19: "images/logo_" + status + ".png",
      38: "images/logo_" + status + ".png",
      128: "images/logo_" + status + ".png"
    }
  });
}